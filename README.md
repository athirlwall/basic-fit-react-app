### Basic Fit React App

## Configuration

Install the Node dependencies using the command:

npm i

Before starting the app, edit the login.js file and
make sure the LOGIN_ENDPOINT constant is set to the 
NodeJS Basic Fit NodeJS API script endpoint. This
defaults to: http://localhost:3001/login

## Starting the App

Start the app using the command:

npm start

Then browse to the app with the URL:

http://localhost:3000

The 'npm start' command will automatically browse to the correct URL.

## Using the app

IMPORTANT: The accompanying Basic Fit NodeJS project must be set up before the app will work.

After browsing to the base URL the Login screen will display. The following all represent valid credentials:

Email: alex@alex.com, Password 123

Email: jack@jack.com, Password 456

Email: eris@eris.com, Password 789

After logging in the app will go to the Home screen and display the name of the user. If the credentials are incorrect a message will be displayed indicating this. A
401 HTTP code will also be visible in the Javascript console.

To test the serving of static files, use the following:

http://localhost:3001/Alex.jpg

## Not done

There were some problems installing JEST/ENZYME easily so I decided to leave out client-app testing since there was a danger that these packages and dependencies would not install at Basic Fit once the repository was cloned. Making it work required resolving somoe conflicts between create-react-app and Babel, something I do not wish to burden my evaluator with.