class UserProfile {
    constructor() {
        this._userName = ''
    }

    get userName() {
        return this._userName
    }

    set userName(value) {
        this._userName = value
    }
}

export default new UserProfile()
