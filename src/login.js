import React from 'react';
import { withRouter } from 'react-router-dom'
import UserProfile from './userprofile'

const LOGIN_ENDPOINT = 'http://localhost:3001/login'


class Login extends React.Component {
    constructor(props) {
        super(props);
        this.login = this.login.bind(this);
    }

    async login(e) {
        e.preventDefault()

        const email = document.getElementById('email').value
        const password = document.getElementById('password').value

        const credentials = {
            email,
            password
        };
          
        const response = await fetch(LOGIN_ENDPOINT, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(credentials)
        });
        
        let json = await response.json();

        if (response.ok) {    
            UserProfile.userName = json.userName
            this.props.history.push("/home")
        } else {
            const errorNode = document.getElementById('error')
            const textNode = document.createTextNode(json.error);
            errorNode.innerHtml = ''
            errorNode.textContent = ''
            errorNode.appendChild(textNode)
            errorNode.classList.remove('invisible') 
        }
    }
    
    render() {
        return <div className="container">
            <div>&nbsp;</div>
            <h2>Login to Basic Fit</h2>
            <div>&nbsp;</div>
            <form>
                <div className="form-group">
                    <label htmlFor="email">Email address</label>
                    <input type="email" className="form-control" id="email" aria-describedby="emailHelp" />
                </div>
                <div className="form-group">
                    <label htmlFor="password">Password</label>
                    <input type="password" className="form-control" id="password" />
                </div>
                <button type="submit" onClick={this.login} className="btn btn-primary">Login</button>
                <div>&nbsp;</div>
                <div id="error" className="alert alert-danger invisible" role="alert">
                    &nbsp;
                </div>
            </form>
        </div>
    }
}

export default withRouter(Login)