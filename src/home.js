import React from 'react';
import { withRouter } from 'react-router-dom';
import UserProfile from './userprofile'

class Home extends React.Component {

    get userName() {
        return UserProfile.userName
    }

    render() {
        return <div className="container">
            <div>&nbsp;</div>
            <h2>Hello {this.userName}</h2>
            <div>&nbsp;</div>
        </div>
    }
}

export default withRouter(Home)
